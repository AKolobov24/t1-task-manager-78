FROM eclipse-temurin:17-jre
WORKDIR /opt
ADD ./target/task-manager-web.war .
EXPOSE 8080
EXPOSE 5701
ENTRYPOINT exec java -jar ./task-manager-web.war
