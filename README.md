# TASK MANAGER WEB

## DEVELOPER

**NAME**: Alexander Kolobov

**E-MAIL**: akolobov@t1-consulting.ru

**URL**: https://t1-consulting.ru/

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: macOS 13.1

## HARDWARE

**CPU**: M1 Pro

**RAM**: 16Gb

**SSD**: 500Gb

## APPLICATION BUILD

```shell script
mvn clean install
```

## APPLICATION RUN

```shell script
java -jar ./target/task-manager-web.war
```