package ru.t1.akolobov.tm.web.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.data.TestProject;
import ru.t1.akolobov.tm.web.marker.UnitCategory;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.service.UserService;

import java.util.List;

@Transactional
@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private UserService userService;

    private final User currentUser = new User("current", "current");
    private final User otherUser = new User("other", "other");

    @Before
    public void addUsers() {
        userService.save(currentUser);
        userService.save(otherUser);
    }

    @After
    public void cleanData() {
        projectService.deleteAllByUserId(currentUser.getId());
        projectService.deleteAllByUserId(otherUser.getId());
    }

    @Test
    public void save() {
        @NotNull final Project project = TestProject.createProject(currentUser);
        projectService.save(project);
        Assert.assertTrue(projectService.findAllByUserId(currentUser.getId()).contains(project));
        Assert.assertEquals(1, projectService.findAllByUserId(currentUser.getId()).size());
    }

    @Test
    public void deleteAllByUserId() {
        @NotNull final List<Project> projectList = TestProject.createProjectList(currentUser);
        @NotNull final List<Project> user2ProjectList = TestProject.createProjectList(otherUser);
        projectList.forEach(projectService::save);
        user2ProjectList.forEach(projectService::save);
        long size = projectService.findAll().size();
        projectService.deleteAllByUserId(currentUser.getId());
        Assert.assertEquals(
                size - projectList.size(),
                projectService.findAll().size()
        );
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> user1ProjectList = TestProject.createProjectList(currentUser);
        @NotNull final List<Project> user2ProjectList = TestProject.createProjectList(otherUser);
        user1ProjectList.forEach(projectService::save);
        user2ProjectList.forEach(projectService::save);
        Assert.assertEquals(user1ProjectList, projectService.findAllByUserId(currentUser.getId()));
        Assert.assertEquals(user2ProjectList, projectService.findAllByUserId(otherUser.getId()));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final Project project = TestProject.createProject(currentUser);
        projectService.save(project);
        @NotNull final String projectId = project.getId();
        Assert.assertEquals(project, projectService.findByUserIdAndId(currentUser.getId(), projectId));
        Assert.assertNull(projectService.findByUserIdAndId(otherUser.getId(), projectId));
    }

    @Test
    public void deleteByIdAndUserId() {
        TestProject.createProjectList(currentUser).forEach(projectService::save);
        @NotNull final Project project = TestProject.createProject(currentUser);
        projectService.save(project);
        projectService.deleteByUserIdAndId(currentUser.getId(), project.getId());
        Assert.assertNull(projectService.findByUserIdAndId(currentUser.getId(), project.getId()));
    }

}
