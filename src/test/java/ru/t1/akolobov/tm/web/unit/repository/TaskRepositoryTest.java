package ru.t1.akolobov.tm.web.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.web.data.TestTask;
import ru.t1.akolobov.tm.web.marker.UnitCategory;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.repository.TaskRepository;
import ru.t1.akolobov.tm.web.repository.UserRepository;

import java.util.List;

@Transactional
@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskRepositoryTest {

    @Autowired
    private TaskRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    private final User currentUser = new User("current", "current");
    private final User otherUser = new User("test", "test");

    @Before
    public void addUsers() {
        userRepository.save(currentUser);
        userRepository.save(otherUser);
    }

    @After
    public void cleanData() {
        projectRepository.deleteAll();
    }

    @Test
    public void save() {
        @NotNull final Task project = TestTask.createTask(currentUser);
        projectRepository.save(project);
        Assert.assertTrue(projectRepository.findAllByUserId(currentUser.getId()).contains(project));
        Assert.assertEquals(1, projectRepository.findAllByUserId(currentUser.getId()).size());
    }

    @Test
    public void deleteAllByUserId() {
        @NotNull final List<Task> projectList = TestTask.createTaskList(currentUser);
        @NotNull final List<Task> user2TaskList = TestTask.createTaskList(otherUser);
        projectList.forEach(projectRepository::save);
        user2TaskList.forEach(projectRepository::save);
        long size = projectRepository.count();
        projectRepository.deleteAllByUserId(currentUser.getId());
        Assert.assertEquals(
                size - projectList.size(),
                projectRepository.count()
        );
    }

    @Test
    public void existsByUserIdAndId() {
        @NotNull final Task project = TestTask.createTask(currentUser);
        projectRepository.save(project);
        Assert.assertTrue(projectRepository.existsByUserIdAndId(currentUser.getId(), project.getId()));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(otherUser.getId(), project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> user1TaskList = TestTask.createTaskList(currentUser);
        @NotNull final List<Task> user2TaskList = TestTask.createTaskList(otherUser);
        user1TaskList.forEach(projectRepository::save);
        user2TaskList.forEach(projectRepository::save);
        Assert.assertEquals(user1TaskList, projectRepository.findAllByUserId(currentUser.getId()));
        Assert.assertEquals(user2TaskList, projectRepository.findAllByUserId(otherUser.getId()));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final Task project = TestTask.createTask(currentUser);
        projectRepository.save(project);
        @NotNull final String projectId = project.getId();
        Assert.assertEquals(project, projectRepository.findByUserIdAndId(currentUser.getId(), projectId));
        Assert.assertNull(projectRepository.findByUserIdAndId(otherUser.getId(), projectId));
    }

    @Test
    public void deleteByIdAndUserId() {
        TestTask.createTaskList(currentUser).forEach(projectRepository::save);
        @NotNull final Task project = TestTask.createTask(currentUser);
        projectRepository.save(project);
        projectRepository.deleteByUserIdAndId(currentUser.getId(), project.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(currentUser.getId(), project.getId()));
    }

}
