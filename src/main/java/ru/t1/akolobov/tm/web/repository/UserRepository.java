package ru.t1.akolobov.tm.web.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.web.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull final String login);

    boolean existsByLogin(@NotNull final String login);

}
