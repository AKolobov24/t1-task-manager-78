package ru.t1.akolobov.tm.web.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.akolobov.tm.web.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractModel {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name = "New Model";

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @XmlSchemaType(name = "string")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_start")
    @XmlSchemaType(name = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    @XmlSchemaType(name = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    @Nullable
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@Nullable final Date dateStart) {
        this.dateStart = dateStart;
    }

    @Nullable
    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@Nullable final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@NotNull final User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractModel that = (AbstractModel) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                status == that.status &&
                (
                        (user != null && that.user != null &&
                                user.getLogin().equals(that.user.getLogin())
                        ) || (user == null && that.user == null)
                );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
