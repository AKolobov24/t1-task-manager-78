package ru.t1.akolobov.tm.web.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Result {

    private final boolean success;

    @Nullable
    private String message;

    public Result() {
        this.success = true;
    }

    public Result(boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception exception) {
        this.success = false;
        this.message = exception.getMessage();
    }

    public boolean isSuccess() {
        return success;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

}
